import './App.css';
import React, { Component } from 'react'
import Menu from './components/Menu';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home/Home';
import Video from './components/_Video/Video';
import Account from './components/Account/Account';
import Auth from './components/_Auth/Auth';
import ReactRouter from './components/ReactRouter';
import NotFound from './components/NotFound';
import Posts from './components/Home/Posts';
import Welcome from './components/_Auth/Welcome';

export default class App extends Component {
  constructor(){
    super();
    this.state={
      data: [{
        id: 1,
        title: 'MacBook Pro (16-inch, 2019)',
        description: 'A new Magic Keyboard, larger display, better sound and faster performance make the 16-inch MacBook Pro an excellent choice for power users.',
        image: 'https://cdn.mos.cms.futurecdn.net/PdALFp4rRnhLqqmDrDmTZ7.jpg'
      },
      {
        id: 2,
        title: 'Dell XPS (15-inch, 2020)',
        description: 'Good news if you missed Dell Labor Day sale earlier this week. The retailer is doubling down on some of its sales and taking 17% off its best laptops Sale.',
        image: 'https://cdn.mos.cms.futurecdn.net/XjHSftqNdB48DVTaHtjZQi.jpg'
      },
      {
        id: 3,
        title: 'Razer Blade 15 Advanced Model (Mid-2019, OLED)',
        description: 'The first major wave of laptops with OLED displays is here, and the results are stunning.',
        image: 'https://i.pcmag.com/imagery/reviews/063DceJdhWJEZJOlVmBF0TN-29.v_1569469931.jpg '
      },
      {
        id: 4,
        title: 'Best 2-in-1 laptops 2020',
        description: 'The best 2-in-1 laptops are the perfect solution for those yearning for something a little bit different from the traditional form-factor of most laptops.',
        image: 'https://cdn.mos.cms.futurecdn.net/NWS2KsmEBXWEpCiSa8Ch6Z.png'
      },
      {
        id: 5,
        title: 'MSI Prestige (15-inch)',
        description: 'The MSI Prestige offers portability, power and endurance in an attractive chassis targeting content creators.',
        image: 'https://cdn.mos.cms.futurecdn.net/BgzDcncTxtEvj47NiprnbS.jpg'
      },
      {
        id: 6,
        title: 'HP Spectre x360 15 (2020)',
        description: 'HP’s just announced Spectre x360 15 (2020) has super slim bezels, presenting an even more gorgeous design to show off its impressive 4K OLED screen.',
        image: 'https://cdn.mos.cms.futurecdn.net/TfuDEG9ovCdkifvHAkrmbG.jpg'
      }]
    }
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Menu />
          <Switch>
            <Route path='/' exact component={ReactRouter} />
            <Route path='/home' render={()=><Home data={this.state.data}/>} />
            <Route path='/Video' component={Video} />
            <Route path='/Account' component={Account} />
            <Route path='/Auth' component={Auth} />
            <Route path='/Posts/:id' render={(props)=><Posts {...props} data={this.state.data}/>}/>
            <Route path='/Welcome' component={Welcome}/>
            <Route path='*' component={NotFound}/>
          </Switch>
        </Router>
      </div>
    )
  }
}
