import React from 'react';
// import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";

function Account() {
    return (
        <div className="container animation">
            <Router>
                <div>
                    <h2>Accounts</h2>

                    <ul>
                        <li>
                            <Link to="/netflix">Netflix</Link>
                        </li>
                        <li>
                            <Link to="/zillow-group">Zillow Group</Link>
                        </li>
                        <li>
                            <Link to="/yahoo">Yahoo</Link>
                        </li>
                        <li>
                            <Link to="/modus-create">Modus Create</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route path="/:id" children={<Child />} />
                    </Switch>
                </div>
            </Router>
        </div>
    )
}
function Child() {
    let { id } = useParams();
    return (
      <div>
        <h3>The <span className="name">name</span> in the query string is "{id}"</h3>
      </div>
    );
}
export default Account
