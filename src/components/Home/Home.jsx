import React from 'react';
import { Card, CardDeck } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Home({ data }) {
    let displayData = data.map((data) =>
        <CardDeck key={data.id} className="col-md-4 Scard">
            <Card>
                <Card.Img variant="top" src={data.image} />
                <Card.Body>
                    <Card.Title>{data.title}</Card.Title>
                    <Card.Text>{data.description}</Card.Text>
                    <Card.Link as={Link} to={`/posts/${data.id}`}>See More</Card.Link>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                </Card.Footer>
            </Card>
        </CardDeck>)
    return (
        <div className="container">
            <div className="row">
                {displayData}
            </div>
        </div >
    )
}

export default Home
