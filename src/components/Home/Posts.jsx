import React from 'react'

function Posts({ match, data }) {
    let showIndexId = data.find((d) => d.id == match.params.id)
    return (
        <div className="container">
            <div className="row">
                <p>This is content from post {showIndexId.id}</p>
            </div>
        </div>
    )
}

export default Posts
