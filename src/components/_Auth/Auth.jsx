import React from 'react';
import { Form, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
function Auth() {
    return (
        <div className="container animation">
            <Form>
                <Form.Row>
                    <Col>
                        <Form.Control placeholder="Username" />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Password" />
                    </Col>
                    <Col>
                        <Link to='/welcome'><Button variant="primary" type="submit">Submit</Button></Link>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    )
}


export default Auth
