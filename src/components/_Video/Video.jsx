import React from "react";
import { BrowserRouter as Router, Switch, Route, Link, useParams, useRouteMatch } from "react-router-dom";

export default function NestingExample() {
    return (
        <div className="container animation">
            <Router>
                <ul>
                    <li>
                        <Link to="/animation">Animation</Link>
                    </li>
                    <li>
                        <Link to="/movie">Movie</Link>
                    </li>
                </ul>
                <Switch>
                    <Route path="/animation">
                        <Animation />
                    </Route>
                    <Route path="/movie">
                        <Movie />
                    </Route>
                    <h2>Please Select A Topic.</h2>
                </Switch>
            </Router>
        </div>
    );
}
function Animation() {
    var { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Animation Category</h2>
            <ul>
                <li>
                    <Link to={`${url}/action`}>Action</Link>
                </li>
                <li>
                    <Link to={`${url}/romance`}>Romance</Link>
                </li>
                <li>
                    <Link to={`${url}/comedy`}>Comedy</Link>
                </li>
            </ul>
            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <PartTwo />
                </Route>
            </Switch>
        </div>
    );
}

function Movie() {
    var { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Movie Category</h2>
            <ul>
                <li>
                    <Link to={`${url}/aventure`}>Aventure</Link>
                </li>
                <li>
                    <Link to={`${url}/comedy`}>Comedy</Link>
                </li>
                <li>
                    <Link to={`${url}/crime`}>Crime</Link>
                </li>
                <li>
                    <Link to={`${url}/documentory`}>Documentory</Link>
                </li>
            </ul>
            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <PartTwo />
                </Route>
            </Switch>
        </div>
    );
}
function PartTwo() {
    let { topicId } = useParams();
    return (
        <div>
            <h3>{topicId}</h3>
        </div>
    );
}
